# Docker [![pipeline status](https://gitlab.com/Xvlad/docker/badges/master/pipeline.svg)](https://gitlab.com/Xvlad/docker/-/commits/master)
 
My docker images

## Commands for build and push to registry
```bash
# Use tag for registry name
docker build . -t xvlad/docker-ci:latest
docker push xvlad/docker-ci:latest
```